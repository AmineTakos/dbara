import {View, Text, Image, TouchableOpacity, Alert} from 'react-native';
import React from 'react';
import styles from './Card.style';
import {COLORS, FONT, images} from '../../constants';
import LinearGradient from 'react-native-linear-gradient';

type props = {
  imgUrl: any;
  name: string;
  location: string;
  dispo: boolean;
  rating: number;
  click: () => void;
  height?: number;
  pers?: number;
  distance?: number;
};

const Card = (props: props) => {
  return (
    <TouchableOpacity style={styles.container} onPress={props.click}>
      <Image
        source={props.imgUrl}
        style={{height: props.height || 200, ...styles.image}}
      />
      <View style={styles.ratingContainer}>
        <Image source={images.star} style={styles.star} />
        <Text style={styles.rating}>{`${props.rating}`}</Text>
      </View>
      <View style={styles.dispo}>
        <Text style={props.dispo ? styles.dispoText : styles.runOut}>
          {props.dispo ? 'Dispo' : 'RunOut'}
        </Text>
      </View>
      <View style={styles.textContainer}>
        <View>
          <Text
            style={{
              fontFamily: FONT.bold,
              fontSize: 18,
              color: COLORS.textColor,
            }}>
            {props.name}
          </Text>
          <Text
            style={{
              fontFamily: FONT.medium,
              fontSize: 13,
              color: COLORS.subText,
            }}>
            {props.location}
          </Text>
        </View>
        <View style={styles.peersContainer}>
          <LinearGradient
            start={{x: 0, y: 0}}
            end={{x: 1, y: 0}}
            colors={[COLORS.orange, COLORS.red]}
            style={styles.option}>
            <Text style={styles.pers}>{`${
              props.pers?.toString() || 6
            } pers`}</Text>
          </LinearGradient>
          <Text style={styles.km}>{`${
            props.distance?.toString() || 1.2
          } km`}</Text>
        </View>
      </View>
    </TouchableOpacity>
  );
};

export default Card;
