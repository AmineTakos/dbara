import {StyleSheet} from 'react-native';
import {COLORS, FONT} from '../../constants';
export default styles = StyleSheet.create({
  container: {
    borderRadius: 12,
    shadowColor: COLORS.subText,
    elevation: 15,
    backgroundColor: 'white',
    display: 'flex',
    flex: 1,
  },
  image: {
    width: '100%',
    flex: 1,
    resizeMode: 'cover',
    borderTopLeftRadius: 12,
    borderTopRightRadius: 12,
  },

  ratingContainer: {
    position: 'absolute',
    right: 10,
    top: 10,
    width: 45,
    height: 20,
    paddingLeft: 5,
    paddingRight: 5,

    display: 'flex',
    justifyContent: 'space-around',
    flexDirection: 'row',
    alignItems: 'center',

    backgroundColor: 'white',

    elevation: 5,
    shadowColor: 'black',
    borderRadius: 5,
  },
  rating: {
    fontFamily: FONT.medium,
    fontSize: 11,
    color: COLORS.subText,
  },
  star: {width: 12, height: 12},
  dispo: {
    position: 'absolute',
    left: 10,
    top: 10,
    width: 45,
    height: 20,
    paddingLeft: 5,
    paddingRight: 5,
    backgroundColor: 'white',

    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',

    elevation: 5,
    shadowColor: 'black',
    borderRadius: 5,
  },
  dispoText: {
    width: 45,
    height: 20,
    fontFamily: FONT.medium,
    fontSize: 11,
    textAlign: 'center',
    color: COLORS.green,
  },
  runOut: {
    width: 45,
    height: 20,
    fontFamily: FONT.medium,
    fontSize: 11,
    textAlign: 'center',
    color: COLORS.red,
  },
  textContainer: {
    height: 65,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginTop: 2,
    marginBottom: 5,
    marginLeft: 5,
    marginRight: 5,
  },
  peersContainer: {
    display: 'flex',
    flexDirection: 'row',
    gap: 5,
  },
  // pers Text  km
  option: {
    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 3,
    borderRadius: 20,
  },
  pers: {
    fontFamily: FONT.medium,
    color: 'white',
    textAlign: 'center',
    fontSize: 12,
  },
  km: {
    backgroundColor: '#848DFF',
    fontFamily: FONT.medium,
    color: 'white',
    textAlign: 'center',
    fontSize: 12,

    paddingLeft: 5,
    paddingRight: 5,
    paddingBottom: 3,
    borderRadius: 20,
  },
});
