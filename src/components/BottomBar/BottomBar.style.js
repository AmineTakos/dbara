import {StyleSheet} from 'react-native';
import {COLORS} from '../../constants';
const styles = StyleSheet.create({
  container: {
    height: 60,
    paddingLeft: 20,
    paddingRight: 20,
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',

    borderTopLeftRadius: 25,
    borderTopRightRadius: 25,
    borderWidth: 0.5,
    borderColor: COLORS.gray,
  },
  image: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
  },
  image2: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    marginRight: 25,
  },
  image3: {
    height: 25,
    width: 25,
    resizeMode: 'contain',
    marginLeft: 25,
  },
  addBtn: {
    width: 70,
    height: 70,
    backgroundColor: COLORS.primary,
    position: 'absolute',
    bottom: 15,
    left: '46%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 50,

    shadowColor: COLORS.primary,
    elevation: 14,
  },
});
export default styles;
