import {View, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import styles from './BottomBar.style';

const iconPath = '../../../assets/icons/bottomBar/';
const BottomBar = () => {
  return (
    <View style={styles.container}>
      <TouchableOpacity>
        <Image
          source={require(`${iconPath}home.png`)}
          style={styles.image}></Image>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          source={require(`${iconPath}bookmark.png`)}
          style={styles.image2}></Image>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          source={require(`${iconPath}notification.png`)}
          style={styles.image3}></Image>
      </TouchableOpacity>
      <TouchableOpacity>
        <Image
          source={require(`${iconPath}profil.png`)}
          style={styles.image}></Image>
      </TouchableOpacity>
      <TouchableOpacity style={styles.addBtn}>
        <Image
          source={require(`${iconPath}plus.png`)}
          style={styles.image}></Image>
      </TouchableOpacity>
    </View>
  );
};

export default BottomBar;
