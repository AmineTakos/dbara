import {StyleSheet} from 'react-native';

import {FONT, SIZES, COLORS, SHADOWS} from '../../constants';

const styles = StyleSheet.create({
  input: {
    fontFamily: FONT.medium,
    flex: 1,
    textAlign: 'center',
    color: COLORS.secondary,
  },
  container: {
    flexDirection: 'row',
    height: 50,
    gap: 10,
    alignItems: 'center',

    paddingLeft: 20,
    paddingRight: 20,
    marginLeft: 10,
    marginRight: 10,

    borderWidth: 1,
    borderRadius: 10,
    borderColor: COLORS.gray,
    backgroundColor: 'white',
    ...SHADOWS.medium,
  },
  icon: {
    height: 20,
    width: 20,
  },
});

export default styles;
