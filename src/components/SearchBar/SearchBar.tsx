import {View, Text, TextInput, Image} from 'react-native';
import React from 'react';
import styles from './searchBar.style';
import {COLORS} from '../../constants';

const SearchBar = () => {
  const [text, onChangeText] = React.useState('');
  const [placeholder, isPlaceholder] = React.useState('Trouver repas');
  const [number, onChangeNumber] = React.useState('');
  return (
    <View style={styles.container}>
      <Image
        style={styles.icon}
        source={require('../../../assets/icons/search.png')}></Image>
      <TextInput
        style={styles.input}
        onChangeText={onChangeText}
        value={text}
        placeholder={placeholder}
        placeholderTextColor={COLORS.secondary}
        onFocus={() => {
          isPlaceholder('');
        }}
        onEndEditing={() => {
          text == '' && isPlaceholder('Trouver repas');
        }}
      />
      <Image
        style={styles.icon}
        source={require('../../../assets/icons/filter.png')}></Image>
    </View>
  );
};

export default SearchBar;
