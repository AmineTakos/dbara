import {StyleSheet} from 'react-native';
import {COLORS, FONT} from '../../constants';

const styles = StyleSheet.create({
  container: {
    height: 60,
    display: 'flex',
    flexDirection: 'row',
    alignItems: 'center',
    paddingLeft: 10,
    paddingRight: 10,
  },
  backImg: {
    height: '50%',
    resizeMode: 'contain',
  },
  titleContainer: {
    position: 'absolute',
    textAlign: 'center',
    width: '100%',
    display: 'flex',
    justifyContent: 'center',
    alignItems: 'center',
  },
  title: {
    fontFamily: FONT.semiBold,
    fontSize: 19,
    color: COLORS.textColor,
  },
});
export default styles;
