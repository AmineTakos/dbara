import {View, Text, TouchableOpacity, Image} from 'react-native';
import React from 'react';
import styles from './TitleBar.style';
import {images} from '../../constants';
import {HomeScreenNavigationProp} from '../../App';
import {useNavigation} from '@react-navigation/native';
const TitleBar = () => {
  const navigation = useNavigation<HomeScreenNavigationProp>();
  return (
    <View style={styles.container}>
      <View style={styles.titleContainer}>
        <Text style={styles.title}>Repas du jour</Text>
      </View>
      <TouchableOpacity onPress={() => navigation.goBack()}>
        <Image source={images.back} style={styles.backImg}></Image>
      </TouchableOpacity>
    </View>
  );
};

export default TitleBar;
