/**
 * Sample React Native App
 * https://github.com/facebook/react-native
 *
 * @format
 */

import {NavigationContainer} from '@react-navigation/native';
import {createNativeStackNavigator} from '@react-navigation/native-stack';
import {useColorScheme} from 'react-native';
import * as React from 'react';
import Home from './screens/Home/Home';
import ListRepas from './screens/ListRepas';

import type {NativeStackNavigationProp} from '@react-navigation/native-stack';
import Detail from './screens/Detail';

export type HomeStackNavigatorParamList = {
  Home: undefined;
  ListRepas: undefined;
  Detail: {
    id: string;
  };
};

export type HomeScreenNavigationProp = NativeStackNavigationProp<
  HomeStackNavigatorParamList,
  'Detail'
>;

const Stack = createNativeStackNavigator<HomeStackNavigatorParamList>();

function App(): JSX.Element {
  const isDarkMode = useColorScheme() === 'dark';
  return (
    <NavigationContainer>
      <Stack.Navigator screenOptions={{headerShown: false}}>
        <Stack.Screen
          name="Home"
          component={Home}
          options={{animation: 'fade'}}
        />
        <Stack.Screen
          name="ListRepas"
          component={ListRepas}
          options={{
            animation: 'slide_from_right',
          }}
        />
        <Stack.Screen
          name="Detail"
          component={Detail}
          options={{
            animation: 'fade',
          }}
        />
      </Stack.Navigator>
    </NavigationContainer>
  );
}

export default App;
