import {COLORS, FONT, SIZES, SHADOWS} from './theme';
import images from './images';
import DATA from './data';
export {COLORS, FONT, SIZES, SHADOWS, images, DATA};
