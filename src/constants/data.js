import images from './images';
const repas = {
  data: [
    {
      id: '1',
      name: 'Couscous osbane',
      rating: 4.5,
      place: 'Ariana',
      distance: 4.5,
      personne: 6,
      dispo: true,
      phone: 2985658759,
      dispoTime: {hour: 12, minute: 45},
      imgPrincipal: images.couscous,
      img: [images.couscous1, images.couscous2, images.couscous3],
    },
    {
      id: '2',
      name: 'Mloukhia',
      rating: 4.4,
      place: 'La Marsa',
      distance: 7,
      personne: 12,
      phone: 2985658759,
      dispo: true,
      dispoTime: {hour: 14, minute: 15},
      imgPrincipal: images.mloukhia,
      img: [],
    },
    {
      id: '3',
      name: 'Calamar farcie',
      rating: 4.2,
      place: 'Grand tunis',
      distance: 5,
      personne: 8,
      phone: 2985658759,
      dispo: true,
      dispoTime: {hour: 15, minute: 30},
      imgPrincipal: images.calamars,
      img: [],
    },
  ],
};
const recette = {
  data: [
    {
      id: '1',
      name: 'Recette Tiramisu',
      rating: 4.5,
      place: 'Ariana',
      distance: 4.5,
      personne: 6,
    },
  ],
};
export default {repas, recette};
