const COLORS = {
  primary: '#5663FF',
  secondary: '#6E7FAA',

  textColor: '#222455',
  subText: '#8A98BA',

  bg: '#fcfcfc',

  gray: '#E8E8E8',
  gray2: '#C1C0C8',

  white: '#F3F4F8',
  lightWhite: '#FAFAFC',

  green: '#4CD964',
  red: '#FF5673',
  orange: '#FF8C48',

  blue: '#848DFF',
};

const FONT = {
  medium: 'JosefinSans-Medium',
  semiBold: 'JosefinSans-SemiBold',
  bold: 'JosefinSans-Bold',
};

const SIZES = {
  xSmall: 10,
  small: 12,
  medium: 16,
  large: 20,
  xLarge: 24,
  xxLarge: 32,
};

const SHADOWS = {
  small: {
    shadowColor: '#000',
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 3.84,
    elevation: 2,
  },
  medium: {
    shadowColor: COLORS.gray,
    shadowOffset: {
      width: 0,
      height: 2,
    },
    shadowOpacity: 0.25,
    shadowRadius: 5.84,
    elevation: 5,
  },
};

export {COLORS, FONT, SIZES, SHADOWS};
