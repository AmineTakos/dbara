import searchIcon from '../../assets/icons/search.png';
import filterIcon from '../../assets/icons/filter.png';
import star from '../../assets/icons/star.png';
import chef from '../../assets/icons/chef.png';
import back from '../../assets/icons/back.png';

import teramisou from '../../assets/images/teramisou.jpg';
import couscous from '../../assets/images/couscous/couscous.png';
import couscous1 from '../../assets/images/couscous/couscous1.jpg';
import couscous2 from '../../assets/images/couscous/couscous2.jpg';
import couscous3 from '../../assets/images/couscous/couscous3.jpg';
import mloukhia from '../../assets/images/mloukhia.jpg';
import calamars from '../../assets/images/calamars/Calamars-farcis-à-la-tunisienne.jpg';

export default {
  searchIcon,
  filterIcon,
  couscous,
  star,
  chef,
  teramisou,
  mloukhia,
  couscous1,
  couscous2,
  couscous3,
  calamars,
  back,
};
