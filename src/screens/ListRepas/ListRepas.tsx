import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import styles from './ListRepat.style';
import TitleBar from '../../components/TitleBar';
import SearchBar from '../../components/SearchBar';
import {DATA} from '../../constants';
import Card from '../../components/Card';
const ListRepas = () => {
  return (
    <SafeAreaView style={styles.globalContainer}>
      <TitleBar />
      <SearchBar />
      <FlatList
        data={DATA.repas.data}
        renderItem={({item}) => (
          <View style={{marginTop: 15, marginLeft: 10, marginRight: 10}}>
            <Card
              imgUrl={item.imgPrincipal}
              dispo={item.dispo}
              location={item.place}
              name={item.name}
              rating={item.rating}
              distance={item.distance}
              height={200}
              pers={item.personne}
              click={() => {}}
            />
          </View>
        )}
        keyExtractor={item => item.id}
      />
    </SafeAreaView>
  );
};

export default ListRepas;
