import {StyleSheet} from 'react-native';
import {COLORS, FONT} from '../../constants';

const styles = StyleSheet.create({
  globalContainer: {backgroundColor: COLORS.bg, height: '100%'},
});
export default styles;
