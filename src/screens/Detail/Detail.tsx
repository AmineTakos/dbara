import {View, Text, Image} from 'react-native';
import React from 'react';

import {SafeAreaView} from 'react-native-safe-area-context';
import type {RouteProp} from '@react-navigation/native';
import {HomeStackNavigatorParamList} from '../../App';
import {useRoute} from '@react-navigation/native';
import {DATA} from '../../constants';
import styles from './Detail.style';

type DetailsScreenRouteProp = RouteProp<HomeStackNavigatorParamList, 'Detail'>;

const Detail = () => {
  const route = useRoute<DetailsScreenRouteProp>();
  return (
    <SafeAreaView>
      <View style={styles.imgContainer}>
        <Image
          style={{resizeMode: 'cover'}}
          source={DATA.repas.data[+route.params.id - 1].imgPrincipal}
        />
      </View>
    </SafeAreaView>
  );
};

export default Detail;
