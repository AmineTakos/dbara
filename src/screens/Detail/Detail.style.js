import {StyleSheet} from 'react-native';

import {FONT, SIZES, COLORS, SHADOWS} from '../../constants';

const styles = StyleSheet.create({
  imgContainer: {
    height: 300,
  },
});

export default styles;
