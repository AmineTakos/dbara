import {
  View,
  Text,
  TouchableOpacity,
  StatusBar,
  FlatList,
  Image,
  ScrollView,
} from 'react-native';
import React from 'react';
import {SafeAreaView} from 'react-native-safe-area-context';
import SearchBar from '../../components/SearchBar';
import BottomBar from '../../components/BottomBar';
import styles from './Home.style';
import Card from '../../components/Card';
import {images, FONT, COLORS, DATA} from '../../constants';
import {HomeScreenNavigationProp} from '../../App';
import {useNavigation} from '@react-navigation/native';

const Home = () => {
  const navigation = useNavigation<HomeScreenNavigationProp>();
  const chefsData = ['amine', 'halim', 'wajih', 'marwen', 'marwenn'];
  const topRepas = DATA.repas.data[0];

  return (
    <SafeAreaView style={styles.globalContainer}>
      <StatusBar
        backgroundColor={COLORS.bg}
        animated
        barStyle={'dark-content'}
      />
      <View style={{marginTop: 10}}>
        <SearchBar />
      </View>
      <ScrollView style={styles.container} endFillColor={COLORS.red}>
        <View style={styles.cardContainer}>
          <View style={styles.header}>
            <Text style={styles.title}>Repas du jour</Text>
            <TouchableOpacity onPress={() => navigation.navigate('ListRepas')}>
              <Text
                style={{
                  fontFamily: FONT.medium,
                  color: COLORS.subText,
                  fontSize: 14,
                }}>
                See all ({DATA.repas.data.length})
              </Text>
            </TouchableOpacity>
          </View>
          <Card
            click={() => {
              navigation.navigate('Detail', {id: topRepas.id});
            }}
            imgUrl={topRepas.imgPrincipal}
            name={topRepas.name}
            location={topRepas.place}
            rating={topRepas.rating}
            dispo={topRepas.dispo}
          />
        </View>
        <View style={styles.cardContainer}>
          <View style={styles.header}>
            <Text style={styles.title}>Recettes</Text>
            <TouchableOpacity>
              <Text
                style={{
                  fontFamily: FONT.medium,
                  color: COLORS.subText,
                  fontSize: 14,
                }}>
                See all (9)
              </Text>
            </TouchableOpacity>
          </View>
          <Card
            click={() => {
              navigation.navigate('Detail', {id: topRepas.id});
            }}
            imgUrl={images.teramisou}
            name="Recette Tiramisu"
            location="Ariana"
            rating={4.5}
            dispo={true}
          />
        </View>
        <View style={styles.chef}>
          <Text style={styles.title}>Chefs mieux côtés</Text>
          <FlatList
            data={chefsData}
            renderItem={({item}) => (
              <TouchableOpacity>
                <Image source={images.chef} style={styles.chefImage} />
              </TouchableOpacity>
            )}
            keyExtractor={item => item}
            horizontal
            contentContainerStyle={styles.listContainer}></FlatList>
        </View>
        <View style={styles.additional}></View>
      </ScrollView>
      <BottomBar />
    </SafeAreaView>
  );
};

export default Home;
