import {StyleSheet} from 'react-native';
import {COLORS, FONT} from '../../constants';

const styles = StyleSheet.create({
  globalContainer: {
    height: '100%',
    backgroundColor: COLORS.bg,
    display: 'flex',
  },
  container: {},
  additional: {height: 30},
  cardContainer: {
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
    flex: 1,
    display: 'flex',
  },
  header: {
    display: 'flex',
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
    marginBottom: 10,
  },
  title: {
    fontFamily: FONT.semiBold,
    fontSize: 20,
    color: COLORS.textColor,
  },
  chef: {
    height: 90,
    marginLeft: 10,
    marginRight: 10,
    marginTop: 10,
  },
  listContainer: {
    width: '100%',
    display: 'flex',
    justifyContent: 'space-evenly',
  },
  chefImage: {
    marginTop: 5,
    height: 50,
    width: 50,
  },
});
export default styles;
